open Lexer
open Lexing
open Parser
open Print_util
open Arg
open Unix
open Test

(* Main parsing function *)
let parse = Parser.prog Lexer.read

(* Currently unused *)
let analyse_from_string s =
  let lexbuf = Lexing.from_string s in
  parse lexbuf

(* Analyse a file, returns the
*  corresponding expression *)
and analyse_from_filename f_name =
  let f_in = open_in f_name in
  let lexbuf = Lexing.from_channel f_in in
  let p = parse lexbuf in
  close_in f_in;
  p

(* Manage files passed as argument *)
let test_files:string list ref = ref []
    
let add_file_name f_name =
  test_files := f_name :: !test_files

let is_file f_name =
  match (stat f_name).st_kind with
    S_REG -> true
  | _ -> false

(* Add the content of tests/ directory 
 * to the files to analyse*)
let add_test_directory () =
  let open Printf in
  let dir_name =
    sprintf "%s/tests/" (getcwd ()) in
  let dh : dir_handle = opendir dir_name in
  let rec aux () =
    try add_file_name (readdir dh);
        aux ()
    with End_of_file -> ()
  in
  aux ();
  (* Filter to get only regular files 
   * i.e. not "." and ".." *)
  test_files :=
    List.filter (fun s -> is_file ("tests/"^s))
                !test_files;
  closedir dh

(* Deal with debug option *)
let debug:bool ref = ref true
                         
let no_debug () = debug := false

(* Parse command-line arguments *)
let parse_arg () =
  let speclist = [ ("-all", Unit add_test_directory,
                    "Pass all test files as argument");
                   ("-no_debug", Unit no_debug,
                    "Do not display pretty printing of "
                   ^"the parsed AST")
                 ]
  and anon_fun = function s -> add_file_name s
  and usage_msg = "exec -all [-no_debug] | "
                 ^"[-all] [-no_debug] file1 [file2 ..]"
  in
  Arg.parse speclist anon_fun usage_msg;
  test_files := List.rev !test_files



(* Main function *)
let () =
  parse_arg ();
  let open Format in
  let len = List.length !test_files in
  open_hvbox 0;
  (* Iterate over the filenames provided *)
  List.iteri
    (fun i f_name ->
      printf "[%d / %d] %s " (i+1) len f_name;
      begin
        try
          let e = analyse_from_filename
                    (sprintf "tests/%s" f_name) in
          let equals_expr =
            try let e2 = List.assoc f_name file_to_expr in
                e = e2
            with Not_found -> true
          in
          begin
            if equals_expr then
              print_green "OK"
            else
              begin
                open_hvbox 4;
                print_string " -- Parsed expression doesn't correspond";
                print_space ();
                printf "take a look at tests/%s for more infos" f_name;
                print_red " X";
                close_box ()
              end
          end;
          print_cut ();
          
          (* If debug pretty-print the expression *)
          if !debug then begin
              print_expr e;
              print_cut (); print_cut ()
            end
        with
        | SyntaxError msg ->
           print_string (" -- SyntaxError: "^msg);
           print_red " X";
           print_cut ();
           if !debug then print_cut ()
        | Parser.Error ->
           print_string " -- Parser.Error";
           print_red " X";
           print_cut ();
           if !debug then print_cut ()
      end
    )
    !test_files;
  close_box ()
                
               
