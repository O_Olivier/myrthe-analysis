EXEC=exec
all:ast parser lexer print_util main
	@ocamlc unix.cma myrtheAST.cmo parser.cmo lexer.cmo print_util.cmo test.cmo main.cmo -o $(EXEC)

ast: myrtheAST.ml
	@ocamlc -c myrtheAST.ml

parser: parser.mly
	menhir --infer $^
	@ocamlc -c $@.mli $@.ml

lexer: lexer.mll
	ocamllex $^
	@ocamlc -c $@.ml

print_util: print_util.ml
	@ocamlc -c $^
main: test.ml main.ml
	@ocamlc -c $^

clean:
	rm *~

proper:
	rm *.cm? $(EXEC)
