%{
open MyrtheAST
%}

(* VALUE *)
%token <int> INT
%token <string> STRING
%token TRUE FALSE

(* IF-THEN-ELSE / CASE-OF *)
%token IF THEN ELSE
%token CASE OF

(* LET-IN *)
%token LET IN

(* OPERATORS *)
%token NOT SUCC
%token TIMES PLUS AND OR MINUS LEQ EQ

(* SYMBOLS *)
%token LEFT_PAR RIGHT_PAR
%token COLON
%token EOF


%start <MyrtheAST.expression> prog
%%

prog: e = expr; EOF { e }

expr:
  (* VALUE *)
  | v = value
      { Const v }

  (* UNOP *)
  | op = unop; e = expr
      { Unop(op,e) }

  (* BINOP *)
  | op = binop; e1 = expr; e2 = expr
      { Binop(op,e1,e2) }

  (* IF-THEN-ELSE *)
  | IF; e_if = expr; THEN; e1 = expr;
    ELSE; e2 = expr
      { If(e_if,e1,e2) }

  (* VAR *)
  | id = STRING
      { Var id }

  (* LET-IN *)
  | LET; id = STRING; EQ;
    e1 = expr; IN; e2 = expr
      { Let(id,e1,e2) }

  (* CASE-OF *)
  | CASE; e_case = expr; OF;
    l = case_list;
    ELSE; COLON;
    e_else = expr
      { Case(e_case,List.rev l,e_else) }

  (* '(' EXPR ')' *)
  | LEFT_PAR; e = expr; RIGHT_PAR
      { e }
  ;

value:
  | i = INT    { Int i }
  | TRUE       { Bool true }
  | FALSE      { Bool false }
  ;
  
unop:
  | NOT        { Not }
  | SUCC       { Succ }
  ;
  
binop:
  | PLUS       { Plus }
  | TIMES      { Times }
  | MINUS      { Minus }
  | AND	       { Et }
  | OR	       { Or }
  | LEQ	       { Leq }
  | EQ	       { Eq }
  ;
  
case_list:
  | (* empty *) { [] }
  | l = case_list;
    v = value; COLON; e = expr
      { (v,e) :: l }
  ;