<h2>Goals of this project</h2>
The goals are to mess around with the language Myrthe.
By messing around I mean principally lexical and syntaxic analysis.

Myrthe is based on a subset of the Ocaml language with a few differences :
* Infix operators (for the moment)
* Case-of instead of pattern-matching

Here are a [lexer](lexer.mll) and a [parser](parser.mly) for it,
as well as [pretty printing utilities](print_util.ml) and [test files](tests).

I have used
[Ocamllex](https://caml.inria.fr/pub/docs/manual-ocaml/lexyacc.html)
for lexing and
[Menhir](http://gallium.inria.fr/~fpottier/menhir/)
for parsing



<h2>The grammar of Myrthe :</h2>

<pre>
value      :=  0 | [1-9][0-9]* | -[1-9][0-9]*  |  true  |  false

var_id     := [a-z][A-Z a-z 0-9 _]*

unop       := not | ++

binop      := + | * | && | || | = | <=

expression :=
  | value
  | unop expression
  | binop expression expression
  | if expression then expression else expression
  | var_id
  | let var_id = expression in expression
  | case expression of case_list
  | ( expression )

case_list  :=
  | value : expression case_list
  | else : expression

</pre>