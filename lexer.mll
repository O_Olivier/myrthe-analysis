{
open Lexing
open Parser

exception SyntaxError of string

let next_line lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_bol = lexbuf.lex_curr_pos;
      	       pos_lnum = pos.pos_lnum + 1 }
}

let int = '0' | '-'? ['1'-'9'] ['0'-'9']*
let var_id = ['a'-'z'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

rule read =
  parse
  | white       { read lexbuf }
  | newline 	{ (* new_line lexbuf; *) read lexbuf }
  | int		{ INT (int_of_string (Lexing.lexeme lexbuf)) }
  | "true"	{ TRUE }
  | "false"	{ FALSE }
  | "not"	{ NOT }
  | "++"	{ SUCC }
  | '+'		{ PLUS }
  | '*'		{ TIMES }
  | "&&"	{ AND }
  | "||"	{ OR }
  | '='		{ EQ }
  | "<="	{ LEQ }
  | '-'		{ MINUS }
  | "("		{ LEFT_PAR }
  | ")"		{ RIGHT_PAR }
  | "if"	{ IF }
  | "then"	{ THEN }
  | "else"	{ ELSE }
  | "let"	{ LET }
  | "in"	{ IN }
  | "case"	{ CASE }
  | "of"	{ OF }
  | ':'		{ COLON }
  | var_id	{ STRING (Lexing.lexeme lexbuf) }
  | eof		{ EOF }
  | _ 		{  raise (SyntaxError ("Unexpected char: "
    	       	     		      ^(Lexing.lexeme lexbuf))) }