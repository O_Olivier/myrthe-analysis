open MyrtheAST
open Format

let red = "\027[31m"
and green = "\027[32m"
and normal = "\027[39;49m"
       
let cbox() = close_box()
let break() = print_break 1 0
let pr = print_string
let print_color col s = print_string (col^s^normal)
let print_red = print_color red
let print_green = print_color green
                          
let pr_value = function
    Int i -> print_int i
  | Bool b -> print_bool b
and pr_unop = function
    Not -> pr "not"
  | Succ -> pr "succ"
and pr_binop = function
    Plus -> pr "+"
  | Times -> pr "*"
  | Et -> pr "and"
  | Or -> pr "or"
  | Eq -> pr "="
  | Leq -> pr "<="
  | Minus -> pr "-"

let rec print_expr = function
  | Const v -> pr_value v
  | Unop (op,e) -> pr_unop op; pr " "; print_expr e
  | Binop (op,e1,e2) -> print_expr e1; pr " ";
                        pr_binop op; pr " ";
                        print_expr e2
  | If (e_if,e1,e2) -> open_hvbox 0;
                       pr "if ";
                       print_expr e_if; break ();
                       pr "then"; print_break 1 2;
                       print_expr e1; break ();
                       pr "else"; print_break 1 2;
                       print_expr e2;
                       cbox ()
  | Var v -> pr v
  | Let(v,e1,e2) -> open_vbox 2;
                    printf "let %s = " v;
                    print_expr e1;
                    pr " in"; break ();
                    print_expr e2;
                    cbox ()
  | Case(e_case,exp_l,e_else) ->
     open_hvbox 2;
     pr "case "; print_expr e_case;
     pr " of"; break ();
     List.iter
       (fun (v,e) -> pr_value v; pr " : ";
                     print_expr e; break ())
       exp_l;
     pr "else : "; print_expr e_else;
     cbox ()
