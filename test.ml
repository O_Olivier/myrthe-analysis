open MyrtheAST

let exp1 = Binop(Et,
                 Binop(Eq,
                       Binop(Plus,Const(Int 1),Const(Int 2)),
                       Const(Int 3)),
                 Const(Bool true))
                
let exp2 = Binop(Or,
                 Binop(Et,
                       Binop(Eq,Const(Int 2),Const(Int 3)),
                       Const(Bool false)),
                 Binop(Eq,
                       Binop(Plus,Const(Int 3),Const(Int 4)),
                       Binop(Plus,Const(Int 1),Const(Int 6))))

let exp3 = Let("x",
               Const(Int 3),
               Let("y",
                   Binop(Plus,
                         Var "x",
                         Const(Int 1)),
                   Let("x",
                       Binop(Eq,
                             Var "x",
                             Const(Int 3)),
                       If(Var "x",
                          Var "y",
                          Const(Int 3)))))

let exp4 = Let("x",
               Binop(Leq,
                     Const(Int 3),
                     Const(Int 4)),
               If(If(Var "x",
                     Const(Bool false),
                     Const(Bool true)),
                  Const(Int 3),
                  Const(Int 4)))
               


let excase1 = Let("b",
                  Const(Int 23),
                  Case(Binop(Leq,
                             Var "b",
                             Const(Int 0)),
                       [(Bool true, Binop(Minus,
                                                Const(Int 0),
                                                Var "b"))],
                       Var "b"))

let excase2 = Case(If(Binop(Leq,
                            Const(Int 4),
                            Const(Int 30)),
                      Binop(Plus,
                            Const(Int 2),
                            Const(Int 3)),
                      Binop(Minus,
                            Const(Int 2),
                            Const(Int 3))),
                   [(Int 3,Const(Int 3));
                    (Int 1,Binop(Plus,
                                        Binop(Times,
                                              Const(Int 6),
                                              Const(Int 100)),
                                        Const(Int 66)))],
                   Const(Int 0))
                                              
let excase3 = Case(Let("b",
                       Binop(Times,
                             Binop(Plus,
                                   Const(Int 30),
                                   Const(Int 21)),
                             Const(Int 26)),
                       Var "b"),
                   [(Int 2,Binop(Leq,
                                        Const(Int 4),
                                        Const(Int 3)));
                    (Int 1,Const(Bool false));
                    (Int 3,Const(Bool true))],
                   Let("x",
                       Const(Int 3),
                       Binop(Leq,
                             Var "x",
                             Var "b")))


let file_to_expr =
  [ ("exp1.txt",exp1);
    ("exp2.txt",exp2);
    ("exp3.txt",exp3);
    ("exp4.txt",exp4);
    ("excase1.txt",excase1);
    ("excase2.txt",excase2);
    ("excase3.txt",excase3) ]
