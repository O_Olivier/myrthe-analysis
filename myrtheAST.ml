type value = Int of int | Bool of bool		    

and var_id = string

and unop = Not | Succ
		   
and binop = Plus | Times | Et | Or | Eq | Leq | Minus
                                                  
and expression =
  | Const of value
  | Unop of unop*expression
  | Binop of binop*expression*expression
  | If of expression*expression*expression
  | Var of var_id
  | Let of var_id*expression*expression
  | Case of expression*(value*expression) list*expression
